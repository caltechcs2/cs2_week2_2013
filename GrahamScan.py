from HullAlgorithm import HullAlgorithm
from math import atan2

class GrahamScan(HullAlgorithm):
    """
    Implements the Graham scan.
    O(n lg n)
    """
    def execute(self):
        def pt(p): return self.plane.points[p]

        def isLeftTurn(p1, p2, p3):
            (a, b), (c, d), (e, f) = pt(p1), pt(p2), pt(p3)
            return a*d - b*c + b*e - d*e + c*f - a*f < 0

        # Step 1: Find p_0
        # TODO: Finish implementing GrahamScan

