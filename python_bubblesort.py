def mysort(l):
    n = len(l)
    swapped = True
    
    while swapped:
        swapped = False
        for i in range(n - 1):
            if l[i] > l[i+1]:
                l[i], l[i+1] = l[i+1], l[i]
                swapped = True
