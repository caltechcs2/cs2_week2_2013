########################################################################
#
# Original code copyright (c) 2011 Ben Yuan <byuan@caltech.edu>
# Modified code copyright (c) 2012 Mike Yurko <myurko@caltech.edu>
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
########################################################################

import matplotlib
matplotlib.use('TkAgg')

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure

import Tkinter as Tk
import sys
import numpy as np
import pyximport
pyximport.install(setup_args={'include_dirs':[np.get_include()]})

# Replace python_bubblesort with the package to test.
import cython_mergesort as cs2sort

# These parameters control the points on which your sort is tested.
# Make sure to set these to 10, 100000, and 5000 before
# running the final plots you will turn in.
PLOT_MIN_SIZE = 10
PLOT_MAX_SIZE = 100000
PLOT_INTERVAL = 5000



import random
import time

# A function to verify that the list is properly sorted.
def is_sorted(a):
    for i in range(len(a) - 1):
        if a[i] > a[i+1]:
            print "I failed on %d"%i
            return False
    return True

# Batch timing of a particular sort function.
def BatchTime(sorter, length, num_runs):
    t = 0
    for i in range(num_runs):
        a = np.random.randint(0, 1000, length)
        c = np.copy(a)
        start = time.clock()
        sorter(a)
        elapsed = time.clock() - start
        t += elapsed
        if not is_sorted(a):
            print "Your sort failed on an array %s"%c
            print "It produced the result       %s"%a
            assert False
    print >> sys.stderr, "Run of length", length, "finished"
    return t / num_runs
    
def main():
    root = Tk.Tk()
    root.wm_title("Performance Testing")
    root.resizable(False, False)

    NUM_RUNS = 5

	# Do the plotting and calculation.
	# If you know matplotlib (http://matplotlib.sourceforge.net/) then
	# you can modify the below to create your own plots.

    f = Figure(figsize=(5,4), dpi=100)
    a = f.add_subplot(111, xlabel='List length', ylabel='Time (s)')
    t = map(lambda x: x, range(PLOT_MIN_SIZE, PLOT_MAX_SIZE, PLOT_INTERVAL))
    s = map(lambda x: BatchTime(cs2sort.mysort, x, NUM_RUNS), t)
    a.plot(t,s)

    # a tk.DrawingArea
    canvas = FigureCanvasTkAgg(f, master=root)
    canvas.show()
    canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

    toolbar = NavigationToolbar2TkAgg( canvas, root )
    toolbar.update()
    canvas._tkcanvas.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

    Tk.mainloop()
    
if __name__ == "__main__":
    main()
